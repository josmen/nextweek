//
//  Int+Extension.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 3/4/23.
//

import Foundation

extension Int {
    func formatted() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        return formatter.string(from: NSNumber(value: self)) ?? ""
    }
}
