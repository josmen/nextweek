//
//  URL+Extension.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 3/4/23.
//

import Foundation

extension Foundation.URL: Swift.Identifiable {
    public var id: URL { return self }
}
