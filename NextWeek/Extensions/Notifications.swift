//
//  Notification.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import Foundation

extension Notification.Name {
    static let eventsDidChange = Notification.Name("EKEXeventsDidChange")
}
