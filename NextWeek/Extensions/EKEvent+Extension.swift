//
//  EKEvent+Extension.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import EventKit
import SwiftUI

extension EKEvent {
    public var id: String {
        return eventIdentifier
    }
    
    var color: Color {
        return Color(UIColor(cgColor: self.calendar.cgColor))
    }
}
