//
//  JSONEncoder+Extensions.swift
//  XLSXParser
//
//  Created by Jose Antonio Mendoza on 4/3/23.
//

import Foundation

extension JSONEncoder {
    static let snakeCaseEncoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        encoder.dateEncodingStrategy = .iso8601
        encoder.outputFormatting = .prettyPrinted
        return encoder
    }()
}
