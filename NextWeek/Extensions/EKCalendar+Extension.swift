//
//  EKCalendar+Extension.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import SwiftUI
import EventKit

extension EventKit.EKCalendar:  Swift.Identifiable {
    public var id: String {
        return self.calendarIdentifier
    }
    
    public var color: Color {
        return Color(UIColor(cgColor: self.cgColor))
    }
    
    public var formattedText: Text {
        return Text("•\u{00a0}")
            .font(.headline)
            .foregroundColor(self.color)
            + Text("\(self.title)")
    }
}
