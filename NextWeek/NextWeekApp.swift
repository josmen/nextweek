//
//  NextWeekApp.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 11/3/23.
//

import SwiftUI

@main
struct NextWeekApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
