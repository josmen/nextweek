//
//  SheetName.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 19/6/24.
//

import Foundation

enum SheetName: String {
    case fecha = "FECHA"
    case maquinistaBenidorm = "G_BENIDORM"
    case usiBenidorm = "USIS BENIDORM"
}
