import Foundation

struct WeekPlan {
    let agent: Int
    let week: [WorkDay]
}

struct WorkDay {
    let shift: Shift
    let date: Date
}

struct Event {
    let title: String
    let startDate: Date
    let endDate: Date
}

extension WorkDay {
    func convertToEvent() -> Event {
        let startDate = Calendar.current.startOfDay(for: date).addingTimeInterval(shift.startTime)
        let endDate = startDate.addingTimeInterval(shift.duration)
        
        return Event(title: "Turno \(self.shift.name)", startDate: startDate, endDate: endDate)
    }
}
