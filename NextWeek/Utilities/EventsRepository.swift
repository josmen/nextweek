//
//  EventsRepository.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import EventKit
import SwiftUI
import Combine

typealias Action = () -> ()

class EventsRepository: ObservableObject {
    static var shared = EventsRepository()
    
    private var subscribers: Set<AnyCancellable> = []
    
    let eventStore = EKEventStore()
    
    @Published var selectedCalendars: Set<EKCalendar>?
    
    @Published var events: [EKEvent]?
    
    var selectedCalendar: EKCalendar {
        if let selectedCalendars {
            return selectedCalendars.first!
        }
        // TODO: quitar el force unwrapping
        return eventStore.defaultCalendarForNewEvents!
    }
    
    private init() {
        selectedCalendars = loadSelectedCalendars()
        
        if selectedCalendars == nil {
            if EKEventStore.authorizationStatus(for: .event) == .fullAccess {
                selectedCalendars = Set([eventStore.defaultCalendarForNewEvents].compactMap({ $0 }))
            }
        }
        
        $selectedCalendars.sink { [weak self] (calendars) in
            self?.saveSelectedCalendars(calendars)
            self?.loadAndUpdateEvents()
        }.store(in: &subscribers)
        
        NotificationCenter.default.publisher(for: .eventsDidChange)
            .sink { [weak self] (notification) in
                self?.loadAndUpdateEvents()
                
            }.store(in: &subscribers)
        
        NotificationCenter.default.publisher(for: .EKEventStoreChanged)
            .sink { [weak self] (notification) in
                self?.loadAndUpdateEvents()
            }
            .store(in: &subscribers)
    }
    
    private func loadSelectedCalendars() -> Set<EKCalendar>? {
        if let identifiers = UserDefaults.standard.stringArray(forKey: "CalendarIdentifiers") {
            let calendars = eventStore.calendars(for: .event).filter({ identifiers.contains($0.calendarIdentifier) })
            guard !calendars.isEmpty else { return nil }
            return Set(calendars)
        } else {
            return nil
        }
    }
    
    private func saveSelectedCalendars(_ calendars: Set<EKCalendar>?) {
        if let identifiers = calendars?.compactMap({ $0.calendarIdentifier }) {
            UserDefaults.standard.set(identifiers, forKey: "CalendarIdentifiers")
        }
    }
    
    private func loadAndUpdateEvents() {
        loadEvents(completion: { (events) in
            DispatchQueue.main.async {
                self.events = events
            }
        })
    }
    
    func requestAccess(onGranted: @escaping Action, onDenied: @escaping Action) {
        eventStore.requestFullAccessToEvents { granted, error in
            if granted {
                onGranted()
            } else {
                onDenied()
            }
        }
    }
    
    func loadEvents(completion: @escaping (([EKEvent]?) -> Void)) {
        requestAccess(onGranted: {
            let weekFromNow = Date().advanced(by: TimeInterval.weeks(2))
            
            let predicate = self.eventStore.predicateForEvents(withStart: Date(), end: weekFromNow, calendars: Array(self.selectedCalendars ?? []))
            
            let events = self.eventStore.events(matching: predicate)
            
            completion(events)
        }) {
            completion(nil)
        }
    }
    
    private func addEventToCalendar(_ event: Event) {
        let eventToAdd = EKEvent(eventStore: eventStore)
        eventToAdd.title = event.title
        eventToAdd.startDate = event.startDate
        eventToAdd.endDate = event.endDate
        eventToAdd.calendar = selectedCalendar
        
        do {
            try eventStore.save(eventToAdd, span: .thisEvent)
        } catch let error {
            print("Error al guardar el evento en el calendario: \(error.localizedDescription)")
        }
    }

    func addEventsToCalendar(_ events: [Event]) {
        requestAccess {
            events.forEach { self.addEventToCalendar($0) }
        } onDenied: {
            print("Acceso denegado")
        }

        
    }
    
    func saveEvents(_ events: [Event]) {
        requestAccess {
            do {
                for event in events {
                    let calendarEvent = EKEvent(eventStore: self.eventStore)
                    calendarEvent.calendar = self.selectedCalendar
                    calendarEvent.title = event.title
                    calendarEvent.startDate = event.startDate
                    calendarEvent.endDate = event.endDate
                    let reminder1 = EKAlarm(relativeOffset: -60 * 60 * 24) // 1 day before event
                    let reminder2 = EKAlarm(relativeOffset: -60 * 60) // 1 hour before event
                    calendarEvent.alarms = [reminder1, reminder2]
                    try self.eventStore.save(calendarEvent, span: .thisEvent)
                }
                try self.eventStore.commit()
            } catch {
                print("❌ Error: \(error)")
            }
        } onDenied: {
            print("Acceso denegado")
        }

    }
    
    deinit {
        subscribers.forEach { (sub) in
            sub.cancel()
        }
    }
}
