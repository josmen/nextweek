//
//  FileManager.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 11/3/23.
//

import CoreXLSX
import Foundation

class FileManager {
    let filepath: String
    let agent: Agent
    let shifts: [Shift]
    
    var agentWeekPlan: WeekPlan?
    
    init(filepath: String, agent: Agent, shifts: [Shift]) {
        self.filepath = filepath
        self.agent = agent
        self.shifts = shifts
    }
    
    func getAgentsWeek() {
        let file = getFile()
        let monday = getDate(from: file)
        
        guard let row = getAgentRow(of: agent, from: file) else { return }
        
        var week: [WorkDay] = []
        let rowShifts = Array(row[2...])
        
        for i in 0...6 {
            let shift = shifts.filter { $0.name == rowShifts[i] }.first
            if let shift {
                let calendarEvent = WorkDay(shift: shift, date: Calendar.current.date(byAdding: .day, value: i, to: monday) ?? Date())
                week.append(calendarEvent)
            }
        }
        
        agentWeekPlan = WeekPlan(agent: agent.cf, week: week)
    }
    
    private func getFile() -> XLSXFile {
        guard let file = XLSXFile(filepath: filepath) else {
            fatalError("XLSX file at \(filepath) is corrupted or does not exist")
        }
        return file
    }
    
    private func getWorksheet(from file: XLSXFile, sheetName: SheetName) throws -> Worksheet? {
        do {
            let workbook = try file.parseWorkbooks()
            for (name, path) in try file.parseWorksheetPathsAndNames(workbook: workbook.first!) {
                guard name == sheetName.rawValue else { continue }
                let worksheet = try file.parseWorksheet(at: path)
                return worksheet
            }
        } catch {
            print("❌ Error: \(error)")
        }
        return nil
    }
    
    private func getDate(from file: XLSXFile) -> Date {
        guard let worksheet = try? getWorksheet(from: file, sheetName: .fecha) else {
            return .now
        }
        let columnCDates = worksheet.cells(atColumns: [ColumnReference("C")!])
            .compactMap { $0.dateValue }
        return columnCDates.first ?? .now
    }
    
    private func getAgentRow(of agent: Agent, from file: XLSXFile) -> [String]? {
        var sheet: SheetName
        
        switch (agent.category, agent.location) {
        case (.maquinista, .benidorm):
            sheet = .maquinistaBenidorm
        case (.usi, .benidorm):
            sheet = .usiBenidorm
        default:
            // TODO: Change fatalError to a notification
            fatalError("This category or location is not implemented yet.")
        }
        
        guard let worksheet = try? getWorksheet(from: file, sheetName: sheet) else {
            print("No se ha encontrado nada de nada")
            return nil
        }
        
        do {
            if let sharedStrings = try file.parseSharedStrings() {
                let rows = worksheet.data?.rows.filter { row in
                    if !row.cells.isEmpty,
                       let firstCellValue = row.cells.first?.stringValue(sharedStrings),
                       firstCellValue.contains(String(agent.cf)) {
                        return firstCellValue.isNumeric
                    }
                    return false
                }
                
                guard rows?.count == 1 else {
                    print("Hay más de una fila que cumple con agente = \(agent.cf)")
                    return nil
                }
                
                let cells = rows![0].cells.filter { $0.value?.isEmpty == false }
                var cellsValues: [String] = []
                let cellsSharedStrings = cells.compactMap { $0.stringValue(sharedStrings)}
                cellsSharedStrings.forEach { cell in
                    if cell.isNumeric {
                        cellsValues.append(String(Int(Double(cell)!)))
                    } else {
                        cellsValues.append(cell)
                    }
                }
                return cellsValues
            }
            return nil
        } catch {
            print("❌ Error: \(error)")
            return nil
        }
    }
}

