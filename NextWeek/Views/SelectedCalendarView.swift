//
//  SelectedCalendarView.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import EventKit
import SwiftUI

struct SelectedCalendarView: View {
    let selectedCalendar: EKCalendar?
    
    var text: Text {
        if let calendar = selectedCalendar?.formattedText {
            return calendar
        } else {
            return Text("Selecciona calendario")
        }
    }
    
    var body: some View {
        HStack {
            text
            Image(systemName: "calendar")
        }
        
    }
}
