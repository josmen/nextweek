//
//  CardView.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 3/4/23.
//

import SwiftUI

struct CardView: View {
    @Environment(\.colorScheme) private var colorScheme
    let date: Date
    let shift: String
    
    var body: some View {
        VStack {
            Text(shift)
                .font(.system(size: 40, weight: .black, design: .rounded))
            Text(date.formatted(date: .abbreviated, time: .omitted))
                .font(.caption)
                .foregroundColor(.secondary)
        }
        .padding()
        .background(colorScheme == .dark ? .black : .white)
        .cornerRadius(8)
        .shadow(radius: 4)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(date: Date(), shift: "3")
    }
}
