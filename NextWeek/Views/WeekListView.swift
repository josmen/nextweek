//
//  WeekListView.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import EventKit
import SwiftUI

struct WeekListView: View {
    @Environment(\.dismiss) private var dismiss
    
    @ObservedObject var eventsRepository = EventsRepository.shared
    
    @State private var week = [WorkDay]()
    @State private var isLoading = false
    
    let agent: Agent
    let filename: URL
    
    let columns = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        NavigationStack {
            VStack {
                if isLoading {
                    ProgressView()
                        .padding(.top)
                } else {
                    Text("Turnos del agente \(agent.cf.formatted())")
                        .font(.title2)
                        .fontWeight(.black)
                        .padding()
                    
                    if week.isEmpty {
                        Spacer()
                        Text("No se ha encontrado ningún turno")
                        Spacer()
                    } else {
                        ScrollView {
                            LazyVGrid(columns: columns, spacing: 16) {
                                ForEach(week, id: \.date) { dayPlan in
                                    CardView(date: dayPlan.date, shift: dayPlan.shift.name)
                                }
                            }
                            .padding(.top)
                        }
                    }
                }
            }
            .padding()
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button("Cancelar") {
                        dismiss()
                    }
                }
                if !week.isEmpty {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Guardar") {
                            insertEvents()
                            dismiss()
                        }
                    }
                }
            }
            .task {
                await populateWeek()
            }
        }
    }
    
    private func populateWeek() async {
        isLoading = true
        let shifts = Shift.shiftsFor(category: agent.category)
        
        let fileManager = FileManager(filepath: filename.relativePath, agent: agent, shifts: shifts)
        fileManager.getAgentsWeek()
        let weekPlan = fileManager.agentWeekPlan
        if let weekPlan { self.week = weekPlan.week }
        isLoading = false
    }
    
    private func insertEvents() {
        let events = week.map { $0.convertToEvent() }
        eventsRepository.addEventsToCalendar(events)
    }
}

struct WeekListView_Previews: PreviewProvider {
    static let url = "/Users/jose/Downloads/16-01_GSEMANAL_2023.xls"
    
    static var previews: some View {
        WeekListView(agent: Agent.agents[0], filename: URL(filePath: url))
    }
}
