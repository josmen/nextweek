//
//  ContentView.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 2/4/23.
//

import EventKit
import SwiftUI

struct ContentView: View {
    enum ActiveSheet {
        case calendarChooser
        case calendarEdit
    }
    
    @ObservedObject var eventsRepository = EventsRepository.shared
    
    @State private var selectedAgent: Agent = Agent.agents[0]
    @State private var showingSheet = false
    @State private var activeSheet: ActiveSheet = .calendarChooser
    @State private var selectedEvent: EKEvent?
    
    @State private var filename: URL?
    @State private var showFileChooser = false
    
    @FocusState private var isFocused: Bool
    
    func showEditFor(_ event: EKEvent) {
        activeSheet = .calendarEdit
        selectedEvent = event
        showingSheet = true
    }
    
    var body: some View {
        NavigationStack {
            VStack {
                List {
                    if let events = eventsRepository.events {
                        ForEach(events, id: \.id) { event in
                            EventRow(event: event).onTapGesture {
                                showEditFor(event)
                            }
                        }
                    } else {
                        Text("No hay eventos en el calendario seleccionado")
                            .font(.headline)
                            .foregroundColor(.secondary)
                    }
                }
                .listStyle(.plain)
                
                VStack(spacing: 20) {
                    Picker("", selection: $selectedAgent) {
                        ForEach(Agent.agents, id: \.cf) { agent in
                            Text(agent.cf.formatted())
                                .tag(agent)
                        }
                    }
                    .pickerStyle(.segmented)
                    
                    SelectedCalendarView(selectedCalendar: Array(eventsRepository.selectedCalendars ?? []).first)
                        .onTapGesture {
                            isFocused = false
                            activeSheet = .calendarChooser
                            showingSheet = true
                        }
                }
                .padding()
                .padding(.bottom)
                .fileImporter(isPresented: $showFileChooser, allowedContentTypes: [.spreadsheet], allowsMultipleSelection: false) { result in
                    do {
                        let fileUrl = try result.get()
                        if fileUrl[0].startAccessingSecurityScopedResource() {
                            self.filename = fileUrl.first
                        }
                    } catch {
                        print("❌ Error importing file: \(error)")
                    }
                }
                .sheet(isPresented: $showingSheet) {
                    if activeSheet == .calendarChooser {
                        CalendarChooser(calendars: $eventsRepository.selectedCalendars, eventStore: eventsRepository.eventStore)
                    } else if activeSheet == .calendarEdit {
                        EventEditView(eventStore: eventsRepository.eventStore, event: selectedEvent)
                    }
                }
                .sheet(item: $filename, onDismiss: {
                    filename?.stopAccessingSecurityScopedResource()
                }, content: { file in
                    WeekListView(agent: selectedAgent, filename: file)
                })
            }
            .navigationTitle("Próxima semana")
            .toolbar {
                ToolbarItem(placement: .bottomBar) {
                    Button {
                        isFocused = false
                        showFileChooser = true
                    } label: {
                        Text("Añadir turnos")
                            .font(.title3)
                            .fontWeight(.semibold)
                            .frame(height: 40)
                            .frame(maxWidth: .infinity)
                    }
                    .tint(.black)
                    .buttonStyle(.borderedProminent)
                    .padding(.bottom, 20)
                }
            }
        }
    }
}
