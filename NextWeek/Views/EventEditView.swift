//
//  EventEditView.swift
//  NextWeek
//
//  Created by Jose Antonio Mendoza on 12/3/23.
//

import SwiftUI
import EventKitUI

struct EventEditView: UIViewControllerRepresentable {
    @Environment(\.dismiss) var dismiss
    
    let eventStore: EKEventStore
    let event: EKEvent?
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<EventEditView>) -> EKEventEditViewController {
        
        let eventEditViewController = EKEventEditViewController()
        eventEditViewController.eventStore = eventStore
        
        if let event = event {
            eventEditViewController.event = event // when set to nil the controller would not display anything
        }
        eventEditViewController.editViewDelegate = context.coordinator
        
        return eventEditViewController
    }
    
    func updateUIViewController(_ uiViewController: EKEventEditViewController, context: UIViewControllerRepresentableContext<EventEditView>) {
        
    }
    
    class Coordinator: NSObject, EKEventEditViewDelegate {
        let parent: EventEditView
        
        init(_ parent: EventEditView) {
            self.parent = parent
        }
        
        func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
            parent.dismiss()
            
            if action != .canceled {
                NotificationCenter.default.post(name: .eventsDidChange, object: nil)
            }
        }
    }
}
